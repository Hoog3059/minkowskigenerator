package minkowski_generator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

public class MinkowskiDiagramPanel extends JPanel {
    private MinkowskiDiagram minkowskiDiagram;

    private int previousMouseX;
    private int previousMouseY;

    boolean drawLightCone = true;
    boolean drawGridS = true;
    boolean drawGridS_ = true;
    boolean drawAxesS = true;
    boolean drawAxesS_ = true;
    boolean drawLabelsS = true;
    boolean drawLabelsS_ = true;

    //Class constructors and arguments are from super class JPanel.
    //region Constuctors
    public MinkowskiDiagramPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);

        this.setBackground(Color.white);
        this.setCursor(new Cursor(Cursor.MOVE_CURSOR));

        try {
            this.minkowskiDiagram = new MinkowskiDiagram(150000000, -10, 10, -10, 10);
        } catch (MinkowskiDiagram.InvalidViewWindow ignored) {
        }

        // Store mouse location when pressed.
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);
                previousMouseX = mouseEvent.getX();
                previousMouseY = mouseEvent.getY();
            }
        });

        // Changes view window when mouse is dragged.
        this.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent mouseEvent) {
                super.mouseDragged(mouseEvent);
                int currentX = mouseEvent.getX();
                int currentY = mouseEvent.getY();

                double scale = 20.0 / (minkowskiDiagram.viewWindow.getXmax() - minkowskiDiagram.viewWindow.getXmin());
                int unitScale = (int) (getHeight() / 20 * scale);

                if (Math.abs((currentX - previousMouseX) / unitScale) >= 1) {
                    int Xmin = getMinkowskiDiagram().viewWindow.getXmin();
                    int Xmax = getMinkowskiDiagram().viewWindow.getXmax();
                    int Ymin = getMinkowskiDiagram().viewWindow.getYmin();
                    int Ymax = getMinkowskiDiagram().viewWindow.getYmax();

                    Xmax = Xmax - ((currentX - previousMouseX) / unitScale);
                    Xmin = Xmin - ((currentX - previousMouseX) / unitScale);

                    // Exception is safe to ignore because code above does not change aspect ratio of view window.
                    try {
                        getMinkowskiDiagram().viewWindow.setViewWindow(Xmin, Xmax, Ymin, Ymax);
                    } catch (MinkowskiDiagram.InvalidViewWindow ignored) {

                    }

                    previousMouseX = currentX;
                }

                if (Math.abs((currentY - previousMouseY) / unitScale) >= 1) {
                    int Xmin = getMinkowskiDiagram().viewWindow.getXmin();
                    int Xmax = getMinkowskiDiagram().viewWindow.getXmax();
                    int Ymin = getMinkowskiDiagram().viewWindow.getYmin();
                    int Ymax = getMinkowskiDiagram().viewWindow.getYmax();

                    Ymax = Ymax + ((currentY - previousMouseY) / unitScale);
                    Ymin = Ymin + ((currentY - previousMouseY) / unitScale);
                    // Exception is safe to ignore because code above does not change aspect ratio of view window.
                    try {
                        getMinkowskiDiagram().viewWindow.setViewWindow(Xmin, Xmax, Ymin, Ymax);
                    } catch (MinkowskiDiagram.InvalidViewWindow ignored) {

                    }

                    previousMouseY = currentY;
                }

                repaint();
            }
        });

        // Zooms diagram when scroll wheel is scrolled.
        this.addMouseWheelListener(e -> {
            int Xmin = getMinkowskiDiagram().viewWindow.getXmin() - Integer.signum(e.getUnitsToScroll());
            int Xmax = getMinkowskiDiagram().viewWindow.getXmax() + Integer.signum(e.getUnitsToScroll());
            int Ymin = getMinkowskiDiagram().viewWindow.getYmin() - Integer.signum(e.getUnitsToScroll());
            int Ymax = getMinkowskiDiagram().viewWindow.getYmax() + Integer.signum(e.getUnitsToScroll());

            // Exception is safe to ignore because the code above doesn't change the aspect ratio of the view window.
            try {
                getMinkowskiDiagram().viewWindow.setViewWindow(Xmin, Xmax, Ymin, Ymax);
            } catch (MinkowskiDiagram.InvalidViewWindow ignored) {

            }

            repaint();
        });
    }

    public MinkowskiDiagramPanel(LayoutManager layout) {
        this(layout, true);
    }

    public MinkowskiDiagramPanel(boolean isDoubleBuffered) {
        this(new FlowLayout(), isDoubleBuffered);
    }

    public MinkowskiDiagramPanel() {
        this(true);
    }
    //endregion

    //region Getters and setters
    public MinkowskiDiagram getMinkowskiDiagram() {
        return minkowskiDiagram;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(this.getParent().getHeight(), this.getParent().getHeight());
    }
    //endregion

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        //region Graphics setup, scaling and translation.
        Graphics2D g2 = (Graphics2D) g;

        // Unit scale = the amount of pixels it takes to fill 1 unit on the graph at scale = 1.
        double unitScale = this.getHeight() / 20.0;

        // The scale to draw the graph at, with scale being 1 when 1 unit is the size of 1 unitScale pixels.
        // The scale is only calculated in the x-direction assuming that the graph with always have a square shape.
        double scale = 20.0 / (this.minkowskiDiagram.viewWindow.getXmax() - this.minkowskiDiagram.viewWindow.getXmin());

        // Translation of the graphic in pixels.
        // Added translation = translation based on the changing view window.
        // Translation = the final translation with added translation adjusted for scale.
        double addedTranslationX = -(this.minkowskiDiagram.viewWindow.getXmax() + this.minkowskiDiagram.viewWindow.getXmin()) / 2.0 * unitScale;
        double addedTranslationY = (this.minkowskiDiagram.viewWindow.getYmax() + this.minkowskiDiagram.viewWindow.getYmin()) / 2.0 * unitScale;
        //                   {Make sure middle point is in the middle}  {translate to account for view window change}
        double translationX = Math.pow(scale, -1) * (unitScale * 10.0) + addedTranslationX;
        double translationY = Math.pow(scale, -1) * (unitScale * 10.0) + addedTranslationY;

        g2.scale(scale, scale);
        g2.translate(translationX, translationY);
        //endregion

        //region Draw grid (x, ct).
        if(this.drawGridS){
            g2.setStroke(new BasicStroke((float) (1.0 * Math.pow(scale, -1))));
            g2.setPaint(Color.gray);

            //     ct grid. Drawn from left to right.
            for (int i = this.getMinkowskiDiagram().viewWindow.getXmin(); i < this.getMinkowskiDiagram().viewWindow.getXmax() + 1; i++) {
                g2.drawLine((int) (unitScale * i), (int) (-this.getMinkowskiDiagram().viewWindow.getYmax() * unitScale), (int) (unitScale * i), (int) (-this.getMinkowskiDiagram().viewWindow.getYmin() * unitScale));
            }

            //     x grid. Drawn from bottom to top.
            for (int i = -this.getMinkowskiDiagram().viewWindow.getYmin(); i > -(this.getMinkowskiDiagram().viewWindow.getYmax() + 1); i--) {
                g2.drawLine((int) (this.getMinkowskiDiagram().viewWindow.getXmin() * unitScale), (int) (unitScale * i), (int) (this.getMinkowskiDiagram().viewWindow.getXmax() * unitScale), (int) (unitScale * i));
            }

        }
        //endregion

        //region Draw axes (x, ct).
        if(this.drawAxesS){
            //     The stroke is adjusted based on the scale to ensure the axes will remain visible.
            g2.setStroke(new BasicStroke((float) (2.0 * Math.pow(scale, -1))));
            g2.setPaint(Color.black);
            //     X-axis:
            g2.drawLine((int) (this.getMinkowskiDiagram().viewWindow.getXmin() * unitScale), 0, (int) (this.getMinkowskiDiagram().viewWindow.getXmax() * unitScale), 0);
            //     Y-axis:
            g2.drawLine(0, (int) (-this.getMinkowskiDiagram().viewWindow.getYmax() * unitScale), 0, (int) (-this.getMinkowskiDiagram().viewWindow.getYmin() * unitScale));
        }
        //endregion

        int fontSize = 10;
        g2.setFont(new Font("SegoeUI", Font.PLAIN, fontSize));

        //region Draw axis labels (x, ct).
        g2.setPaint(Color.black);
        if(this.drawLabelsS){
            //region Draw axis labels (x).
            for (int i = this.getMinkowskiDiagram().viewWindow.getXmin(); i < this.getMinkowskiDiagram().viewWindow.getXmax() + 1; i++) {
                if(i != 0){
                    g2.drawString(String.valueOf(i), (int) (i * unitScale - 5), 10);
                }
            }

            g2.drawString("x", (int) (this.getMinkowskiDiagram().viewWindow.getXmax() * unitScale - 10), -2);
            //endregion

            //region Draw axis labels (ct).
            for (int i = -this.getMinkowskiDiagram().viewWindow.getYmin(); i > -(this.getMinkowskiDiagram().viewWindow.getYmax() + 1); i--) {
                if(i != 0){
                    g2.drawString(String.valueOf(-i), -15, (int) (i * unitScale + 5));
                }
            }

            g2.drawString("ct", 5, (int) -(this.getMinkowskiDiagram().viewWindow.getYmax() * unitScale - 10));
            //endregion
        }
        //endregion

        //region Draw lightcone.
        if(this.drawLightCone){
            g2.setStroke(new BasicStroke((float) (1.0 * Math.pow(scale, -1)), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
            g2.setPaint(Color.black);

            paintLine(g2, this.minkowskiDiagram.viewWindow, unitScale, 1, 0);
            paintLine(g2, this.minkowskiDiagram.viewWindow, unitScale, -1, 0);
        }
        //endregion

        // a and b as in y = ax + b
        double a;
        double b;

        a = this.getMinkowskiDiagram().getBetaFactor();
        //region Draw grid and axis (x').
        g2.setStroke(new BasicStroke((float) (1.0 * Math.pow(scale, -1))));
        g2.setPaint(new Color(255, 125, 125));

        //      x' grid.
        if(this.drawGridS_){
            int bottomRange = -this.getMinkowskiDiagram().viewWindow.getXmax() + this.getMinkowskiDiagram().viewWindow.getYmin();
            int upperRange = -this.getMinkowskiDiagram().viewWindow.getXmin() + this.minkowskiDiagram.viewWindow.getYmax();
            for (int i = bottomRange; i < upperRange; i++) {
                b = (double) (i) / this.getMinkowskiDiagram().getGammaFactor();

                paintLine(g2, this.minkowskiDiagram.viewWindow, unitScale, a, b);
            }
        }

        //      x' axis.
        if(this.drawAxesS_){
            g2.setStroke(new BasicStroke((float) (2.0 * Math.pow(scale, -1))));
            g2.setPaint(Color.red);

            paintLine(g2, this.minkowskiDiagram.viewWindow, unitScale, a, 0);
        }
        //endregion

        //region Draw axis labels (x').
        g2.setPaint(Color.red);
        if(this.drawLabelsS_){
            int maxX_label = Math.round((float) (this.minkowskiDiagram.getViewWindow().getXmax() / this.minkowskiDiagram.getGammaFactor()));
            int minX_label = Math.round((float) (this.minkowskiDiagram.getViewWindow().getXmin() / this.minkowskiDiagram.getGammaFactor()));

            for (int i = minX_label; i < maxX_label; i++) {
                if(i != 0){
                    int x = (int) (i * this.minkowskiDiagram.getGammaFactor() * unitScale);
                    int y = (int) (a * x);

                    g2.drawString(String.valueOf(i), x, -(y - 8));
                }
            }

            int x_Label_xcoord = (int) (this.getMinkowskiDiagram().viewWindow.getXmax() * unitScale - 10);
            int x_Label_ycoord = (int) (x_Label_xcoord * a);
            g2.drawString("x'", x_Label_xcoord, -(x_Label_ycoord + 5));
        }
        //endregion

        a = Math.pow(this.getMinkowskiDiagram().getBetaFactor(), -1);
        //region Draw grid and axis (ct').
        g2.setStroke(new BasicStroke((float) (1.0 * Math.pow(scale, -1))));
        g2.setPaint(new Color(255, 125, 125));

        //      ct' grid.
        if(this.drawGridS_){
            int bottomRange = -this.getMinkowskiDiagram().viewWindow.getXmax() + this.getMinkowskiDiagram().viewWindow.getYmin();
            int upperRange = -this.getMinkowskiDiagram().viewWindow.getXmin() + this.minkowskiDiagram.viewWindow.getYmax();
            for (int i = bottomRange; i < upperRange; i++) {
                b = (double) (i) / (this.getMinkowskiDiagram().getGammaFactor() * this.getMinkowskiDiagram().getBetaFactor());

                paintLine(g2, this.minkowskiDiagram.viewWindow, unitScale, a, b);
            }
        }

        //      ct' axis.
        if(this.drawAxesS_){
            g2.setStroke(new BasicStroke((float) (2.0 * Math.pow(scale, -1))));
            g2.setPaint(Color.red);

            paintLine(g2, this.minkowskiDiagram.viewWindow, unitScale, a, 0);
        }
        //endregion

        //region Draw axis labels (ct').
        g2.setPaint(Color.red);
        if(this.drawLabelsS_){
            int maxY_label = Math.round((float) (this.minkowskiDiagram.getViewWindow().getYmax() / this.minkowskiDiagram.getGammaFactor()));
            int minY_label = Math.round((float) (this.minkowskiDiagram.getViewWindow().getYmin() / this.minkowskiDiagram.getGammaFactor()));

            for (int i = minY_label; i < maxY_label; i++) {
                if(i != 0){
                    int y = (int) (i * this.minkowskiDiagram.getGammaFactor() * unitScale);
                    int x = (int) (y / a);

                    g2.drawString(String.valueOf(i), x - 8, -y);
                }
            }

            int ct_Label_ycoord = (int) (this.minkowskiDiagram.getViewWindow().getYmax() * unitScale - 10);
            int ct_Label_xcoord = (int) (ct_Label_ycoord / a);
            g2.drawString("ct'", ct_Label_xcoord + 8, -ct_Label_ycoord);
        }
        //endregion

        //Old code
        /*
        double scale = 0.5;

        g2.scale(scale, scale);
        g2.translate(0.5 * this.getWidth(), 0.5 * this.getHeight());

        //Draw cross.
        g2.setStroke(new BasicStroke(1));

        g2.drawLine(this.getWidth(), 0, 0, this.getHeight());
        g2.drawLine(0, 0, this.getWidth(), this.getHeight());

        //Draw grid.
        g2.setStroke(new BasicStroke(1));

        double xGridInterval = this.getWidth() / (this.minkowskiDiagram.getXmax() - this.minkowskiDiagram.getXmin());
        double yGridInterval = this.getHeight() / (this.minkowskiDiagram.getYmax() - this.minkowskiDiagram.getYmin());

        //    Vertical lines.
        for (int i = 0; i < this.minkowskiDiagram.getXmax() - this.minkowskiDiagram.getXmin() + 1; i++) {
            g.drawLine((int) Math.ceil((float) xGridInterval * i), 0, (int) Math.ceil((float) xGridInterval * i), this.getHeight());
        }

        //    Horizontal lines.
        for (int i = 0; i < this.minkowskiDiagram.getYmax() - this.minkowskiDiagram.getYmin() + 1; i++) {
            g.drawLine(0, (int) Math.ceil((float) yGridInterval * i), this.getWidth(), (int) Math.ceil((float)yGridInterval * i));
        }

        //Draw axes.
        g2.setStroke(new BasicStroke(2));

        //    X-axis:
        g.drawLine(0, Math.round((float) yGridInterval * this.minkowskiDiagram.getYmax()), this.getWidth(), Math.round((float)yGridInterval * this.minkowskiDiagram.getYmax()));

        //    Y-axis:
        g2.drawLine(Math.round((float) xGridInterval * -this.minkowskiDiagram.getXmin()), 0, Math.round((float) xGridInterval * -this.minkowskiDiagram.getXmin()), this.getHeight());
        */
    }

    /**
     * Paint a line (y = a * x + b) on g2. The function calculates
     * two points (as far apart as possible to ensure the whole line is visible)
     * and draws a line between them.
     * @param g2 A Graphics2D object to draw the line on.
     * @param viewWindow View window to ensure the whole line is visible within this view window.
     * @param unitScale The amount of pixels that is equal to 1 unit of x or y.
     * @param a Parameter a in y = a * x + b.
     * @param b Parameter b in y = a * x + b.
     */
    private void paintLine(Graphics2D g2, MinkowskiDiagram.ViewWindow viewWindow, double unitScale, double a, double b) {
        double yForXMin = (double) (viewWindow.getXmin()) * a + b;
        double yForXMax = (double) (viewWindow.getXmax()) * a + b;

        int x1 = (int) (viewWindow.getXmin() * unitScale);
        int y1 = (int) (-yForXMin * unitScale);
        int x2 = (int) (this.minkowskiDiagram.viewWindow.getXmax() * unitScale);
        int y2 = (int) (-yForXMax * unitScale);

        g2.drawLine(x1, y1, x2, y2);
    }
}

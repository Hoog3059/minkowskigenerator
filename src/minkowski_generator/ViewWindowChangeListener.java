package minkowski_generator;

public interface ViewWindowChangeListener {
    void viewWindowChanged();
}

package minkowski_generator;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MainWindow {
    private JPanel mainPanel;
    private JPanel controlPanel;
    private JTextField txtGammaFactor;
    private JTextField txtBetaFactor;
    private JTextField txtVelocity;
    private JTextField txtPercentageOfC;
    private JButton btnDraw;
    private MinkowskiDiagramPanel minkowskiDiagramPanel;
    private JTextField txtXMin;
    private JTextField txtXMax;
    private JTextField txtYMin;
    private JTextField txtYMax;
    private JCheckBox ckGridS;
    private JCheckBox ckGridS_;
    private JCheckBox ckAxesS;
    private JCheckBox ckAxesS_;
    private JCheckBox ckLabelsS;
    private JCheckBox ckLabelsS_;
    private JCheckBox ckLightCone;
    private JButton btnExport;

    // Stores which JTextField (txtGammaFactor, txtBetaFactor, txtVelocity or txtPercentageOfC) was edited lastly.
    private JTextField lastEditedPhysicsTextField;
    // Stores whether to listen to changes for all JTextFields.
    private boolean listenForTextChanges = true;

    public MainWindow() {
        // Set txtGammaFactor as a placeholder for lastEditedPhysicsTextField.
        lastEditedPhysicsTextField = txtGammaFactor;

        //region Physics text fields onChange handlers.
        // Keeps track of which text field is edited last.
        txtGammaFactor.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onChange();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onChange();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onChange();
            }

            public void onChange() {
                if (listenForTextChanges) {
                    lastEditedPhysicsTextField.setBackground(Color.WHITE);
                    lastEditedPhysicsTextField = txtGammaFactor;
                    lastEditedPhysicsTextField.setBackground(Color.CYAN);
                }
            }
        });

        txtBetaFactor.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onChange(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onChange(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onChange(e);
            }

            public void onChange(DocumentEvent e) {
                if (listenForTextChanges) {
                    lastEditedPhysicsTextField.setBackground(Color.WHITE);
                    lastEditedPhysicsTextField = txtBetaFactor;
                    lastEditedPhysicsTextField.setBackground(Color.CYAN);
                }
            }
        });

        txtVelocity.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onChange(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onChange(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onChange(e);
            }

            public void onChange(DocumentEvent e) {
                if (listenForTextChanges) {
                    lastEditedPhysicsTextField.setBackground(Color.WHITE);
                    lastEditedPhysicsTextField = txtVelocity;
                    lastEditedPhysicsTextField.setBackground(Color.CYAN);
                }
            }
        });

        txtPercentageOfC.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onChange(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onChange(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onChange(e);
            }

            public void onChange(DocumentEvent e) {
                if (listenForTextChanges) {
                    lastEditedPhysicsTextField.setBackground(Color.WHITE);
                    lastEditedPhysicsTextField = txtPercentageOfC;
                    lastEditedPhysicsTextField.setBackground(Color.CYAN);
                }
            }
        });
        //endregion

        btnDraw.addActionListener(actionEvent -> {
            try {
                if (lastEditedPhysicsTextField.equals(txtVelocity)) {
                    minkowskiDiagramPanel.getMinkowskiDiagram().setVelocity(Double.parseDouble(lastEditedPhysicsTextField.getText()));
                } else if (lastEditedPhysicsTextField.equals(txtGammaFactor)) {
                    minkowskiDiagramPanel.getMinkowskiDiagram().setGammaFactor(Double.parseDouble(lastEditedPhysicsTextField.getText()));
                } else if (lastEditedPhysicsTextField.equals(txtBetaFactor)) {
                    minkowskiDiagramPanel.getMinkowskiDiagram().setBetaFactor(Double.parseDouble(lastEditedPhysicsTextField.getText()));
                } else if (lastEditedPhysicsTextField.equals(txtPercentageOfC)) {
                    minkowskiDiagramPanel.getMinkowskiDiagram().setPercentageOfC(Double.parseDouble(lastEditedPhysicsTextField.getText()));
                }

                int Xmin = Integer.parseInt(txtXMin.getText());
                int Xmax = Integer.parseInt(txtXMax.getText());
                int Ymin = Integer.parseInt(txtYMin.getText());
                int Ymax = Integer.parseInt(txtYMax.getText());

                minkowskiDiagramPanel.drawLightCone = ckLightCone.isSelected();
                minkowskiDiagramPanel.drawGridS = ckGridS.isSelected();
                minkowskiDiagramPanel.drawGridS_ = ckGridS_.isSelected();
                minkowskiDiagramPanel.drawAxesS = ckAxesS.isSelected();
                minkowskiDiagramPanel.drawAxesS_ = ckAxesS_.isSelected();
                minkowskiDiagramPanel.drawLabelsS = ckLabelsS.isSelected();
                minkowskiDiagramPanel.drawLabelsS_ = ckLabelsS_.isSelected();

                minkowskiDiagramPanel.getMinkowskiDiagram().viewWindow.setViewWindow(Xmin, Xmax, Ymin, Ymax);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "The inserted number is not valid.", "Invalid number", JOptionPane.ERROR_MESSAGE);
            } catch (MinkowskiDiagram.VelocityTooLargeException ex) {
                JOptionPane.showMessageDialog(null, "The inserted number conflicts with physics by setting a velocity greater than the speed of light (299 792 458 m/s).", "Physics error", JOptionPane.ERROR_MESSAGE);
            } catch (MinkowskiDiagram.InvalidViewWindow ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Invalid view window", JOptionPane.ERROR_MESSAGE);
            }

            updateTextboxValues();

            minkowskiDiagramPanel.repaint();
        });

        btnExport.addActionListener(actionEvent -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Export diagram as PNG");
            fileChooser.setApproveButtonText("Export");
            fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
            fileChooser.removeChoosableFileFilter(fileChooser.getAcceptAllFileFilter());
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PNG file" , "png"));

            int userSelection = fileChooser.showSaveDialog(null);

            if(userSelection == JFileChooser.APPROVE_OPTION){
                BufferedImage image = new BufferedImage(this.minkowskiDiagramPanel.getWidth(), this.minkowskiDiagramPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D g2 = image.createGraphics();
                this.minkowskiDiagramPanel.paintComponent(g2);

                try {
                    File chosenFile = fileChooser.getSelectedFile();

                    if(!chosenFile.getName().endsWith(".png")){
                        chosenFile = new File(chosenFile.getAbsolutePath() + ".png");
                    }

                    ImageIO.write(image, "png", chosenFile);
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, "An error occurred when trying to export the image.", "Error exporting image", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        minkowskiDiagramPanel.getMinkowskiDiagram().addViewWindowChangeListener(this::updateTextboxValues);

        updateTextboxValues();
    }

    public static void main(String[] args) {
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException ignored){

        }

        JFrame frame = new JFrame("Minkowski Generator by Timo Hoogenbosch");
        frame.setContentPane(new MainWindow().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(600, 600));
        frame.pack();
        frame.setVisible(true);

        // Further setup of components is automatically handled by Intellij.
    }

    public void createUIComponents() {
        minkowskiDiagramPanel = new MinkowskiDiagramPanel();
        minkowskiDiagramPanel.setCursor(new Cursor(Cursor.MOVE_CURSOR));

        // Further creating of components is automatically handled by Intellij.
    }

    /**
     * Updates the all textbox values based on the set values in MinkowskiDiagramPanel.MinkowskiDiagram.
     */
    public void updateTextboxValues() {
        listenForTextChanges = false;

        txtVelocity.setText(String.valueOf(minkowskiDiagramPanel.getMinkowskiDiagram().getVelocity()));
        txtBetaFactor.setText(String.valueOf(roundOnXDecimals(minkowskiDiagramPanel.getMinkowskiDiagram().getBetaFactor(), 4)));
        txtGammaFactor.setText(String.valueOf(roundOnXDecimals(minkowskiDiagramPanel.getMinkowskiDiagram().getGammaFactor(), 4)));
        txtPercentageOfC.setText(String.valueOf(roundOnXDecimals(minkowskiDiagramPanel.getMinkowskiDiagram().getPercentageOfC(), 4)));

        txtXMin.setText(String.valueOf(minkowskiDiagramPanel.getMinkowskiDiagram().viewWindow.getXmin()));
        txtXMax.setText(String.valueOf(minkowskiDiagramPanel.getMinkowskiDiagram().viewWindow.getXmax()));
        txtYMin.setText(String.valueOf(minkowskiDiagramPanel.getMinkowskiDiagram().viewWindow.getYmin()));
        txtYMax.setText(String.valueOf(minkowskiDiagramPanel.getMinkowskiDiagram().viewWindow.getYmax()));

        listenForTextChanges = true;
    }

    /**
     * Round a double on a set number of decimals.
     * @param number The number to round.
     * @param decimals The amount of decimals.
     * @return The rounded double.
     */
    private static double roundOnXDecimals(double number, int decimals) {
        return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
    }
}


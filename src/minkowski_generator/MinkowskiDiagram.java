package minkowski_generator;

import java.util.ArrayList;

public class MinkowskiDiagram {
    private final ArrayList<ViewWindowChangeListener> listeners = new ArrayList<>();

    public static final int SPEED_OF_LIGHT = 299792458;
    private double velocity;
    public ViewWindow viewWindow;

    public MinkowskiDiagram(double velocity, int xmin, int xmax, int ymin, int ymax) throws InvalidViewWindow {
        this.velocity = velocity;
        this.viewWindow = new ViewWindow(xmin, xmax, ymin, ymax);
    }

    //region Getters and setters.
    /**
     * Set the velocity for the Minkowski diagram. Returns whether setting was succesful.
     * @param velocity The velocity to set (m/s).
     * @throws VelocityTooLargeException Throws exception if the value is larger than MinkowskiDiagram.SPEED_OF_LIGHT.
     */
    public void setVelocity(double velocity) throws VelocityTooLargeException {
        if (velocity > SPEED_OF_LIGHT) {
            throw new VelocityTooLargeException("The set value conflicts with the rule that the set velocity cannot be larger than lightspeed.");
        } else {
            this.velocity = velocity;
        }
    }

    public double getVelocity() {
        return velocity;
    }

    public void setGammaFactor(double gamma_factor) throws VelocityTooLargeException {
        double velocity = Math.sqrt(-(Math.pow(gamma_factor, -2) - 1) * Math.pow(MinkowskiDiagram.SPEED_OF_LIGHT, 2));
        this.setVelocity(velocity);
    }

    /**
     * Calculate and the return gamma (Lorentz) factor. (See https://en.wikipedia.org/wiki/Lorentz_factor for more information).
     *
     * @return The gamma (Lorentz) factor. (double)
     */
    public double getGammaFactor() {
        return Math.pow(Math.sqrt(1 - (Math.pow(this.velocity, 2) / Math.pow(SPEED_OF_LIGHT, 2))), -1);
    }

    public void setBetaFactor(double betaFactor) throws VelocityTooLargeException {
        double velocity = betaFactor * MinkowskiDiagram.SPEED_OF_LIGHT;
        this.setVelocity(velocity);
    }

    public double getBetaFactor() {
        return this.velocity / MinkowskiDiagram.SPEED_OF_LIGHT;
    }

    public void setPercentageOfC(double percentageOfC) throws VelocityTooLargeException{
        this.setVelocity(percentageOfC / 100 * MinkowskiDiagram.SPEED_OF_LIGHT);
    }

    public double getPercentageOfC(){
        return this.velocity / MinkowskiDiagram.SPEED_OF_LIGHT * 100;
    }

    public ViewWindow getViewWindow(){
        return this.viewWindow;
    }
    //endregion

    public void addViewWindowChangeListener(ViewWindowChangeListener listenerToAdd){
        listeners.add(listenerToAdd);
    }

    public class ViewWindow{
        private int Xmin;
        private int Xmax;
        private int Ymin;
        private int Ymax;

        public ViewWindow(int xmin, int xmax, int ymin, int ymax) throws InvalidViewWindow {
            this.setViewWindow(xmin, xmax, ymin, ymax);
        }

        public int getXmin() {
            return Xmin;
        }

        public int getXmax() {
            return Xmax;
        }

        public int getYmin() {
            return Ymin;
        }

        public int getYmax() {
            return Ymax;
        }

        public void setViewWindow(int Xmin, int Xmax, int Ymin, int Ymax) throws InvalidViewWindow{
            if(Xmin >= Xmax){
                throw new InvalidViewWindow("Xmin cannot be equal or greater than Xmax.");
            }else if(Ymin >= Ymax){
                throw new InvalidViewWindow("Ymin cannot be equal or greater than Ymax.");
            }else if(Xmax - Xmin != Ymax - Ymin){
                throw new InvalidViewWindow("View window has to be a square (Xmax - Xmin must equal Ymax - Ymin).");
            }else{
                this.Xmin = Xmin;
                this.Xmax = Xmax;
                this.Ymin = Ymin;
                this.Ymax = Ymax;

                System.out.println(this.Xmin + " " + this.Xmax + " " + this.Ymin + " " + this.Ymax);

                for(ViewWindowChangeListener listener : MinkowskiDiagram.this.listeners){
                    listener.viewWindowChanged();
                }
            }
        }
    }

    public static class VelocityTooLargeException extends Exception{
        public VelocityTooLargeException(String errorMessage){
            super(errorMessage);
        }
    }

    public static class InvalidViewWindow extends Exception{
        public InvalidViewWindow(String errorMessage){
            super(errorMessage);
        }
    }
}
